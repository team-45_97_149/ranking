class StudentComparison:
    def __init__(self, filepath: str):
        self.students = self.load_students(filepath)
    
    def load_students(self, filepath: str):
        students = {}
        with open(filepath, 'r') as file:
            for line in file:
                parts = line.strip().split()
                name = parts[0]
                marks = list(map(int, parts[1:]))
                students[name] = marks
        return students
    
    def compare(self, student_a, student_b):
        marks_a = self.students[student_a]
        marks_b = self.students[student_b]
        a_better = all(a > b for a, b in zip(marks_a, marks_b))
        b_better = all(b > a for a, b in zip(marks_a, marks_b))
        
        if a_better:
            return f'{student_a} > {student_b}'
        elif b_better:
            return f'{student_b} > {student_a}'
        else:
            return f'{student_a} # {student_b}'
    
    def get_comparisons(self):
        comparisons = []
        names = list(self.students.keys())
        for i in range(len(names)):
            for j in range(i + 1, len(names)):
                comparisons.append(self.compare(names[i], names[j]))
        return comparisons


filepath = "input.txt"
comparison = StudentComparison(filepath)
comparisons = comparison.get_comparisons()
for comparison in comparisons:
    print(comparison)
